#
# Script to pull random cat facts
#
import praw
import random
import requests

def get_cat_fact():
    url = "https://catfact.ninja/facts"
    response = requests.get(
        url,
        headers={"Accept": "application/json"},
    )
    data = response.json()
    data = data['data'][0]["fact"]
    return data

def get_cat_image():
    reddit = praw.Reddit(client_id='svDgXm8wdECQaw',
                         client_secret='jwlqqfGXXx1ZV2N6Jxl0JyCcShg',
                         password='George456',
                         user_agent='Cat_bot by /u/gonerlover',
                         username='gonerlover')
    links = []
    for link in reddit.subreddit('cats').hot(limit=60):
        if link.stickied is False:
            links.append(link.url)
    
    cat_image_link = (links[random.randint(0, 60)])
    return cat_image_link

def get_cat_gif():
    search = "cats"
    url = "https://api.giphy.com/v1/gifs/search"
    response = requests.get(
        url,
        headers={"Accept": "application/json"},
        params={
            "apikey": '7Q4qys4jX7eEh6TadCnJEYiGXN9XP4Y6',
            "q": search,
            "limit": 100
        }
    )
    datalinks = response.json()['data']
    datalink = datalinks[(random.randint(1,100))]['bitly_url']
    return datalink